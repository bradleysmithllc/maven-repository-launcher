package org.bitbucket.bradleysmithllc.maven_repository_launcher.test;

import org.bitbucket.bradleysmithllc.maven_repository_launcher.Launcher;
import org.junit.Test;

public class LauncherTest
{
	//@Test
	public void run()
	{
		Launcher.main(new String [] {
			"-gid",
			"commons-cli",
			"-aid",
			"commons-cli",
			"--version",
			"1.2",
			"-cl",
			"org.apache.commons.cli.CommandLine",
			"-lr",
			"/tmp/repo3",
			"--repository-proxy",
			"http://dvn-artifactory.centralus.cloudapp.azure.com/artifactory/libs-release",
			"--repository-username",
			"developer",
			"--repository-password",
			"Hi8NQp8N$3/2DB,sT}kR"
		});
	}
}