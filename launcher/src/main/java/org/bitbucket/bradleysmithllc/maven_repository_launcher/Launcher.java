package org.bitbucket.bradleysmithllc.maven_repository_launcher;

import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;
import org.bitbucket.bradleysmithllc.java_cl_parser.CommonsCLILauncher;
import org.eclipse.aether.examples.aether.AetherDemo;
import org.eclipse.aether.resolution.DependencyResolutionException;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;

@CLIEntry(
	version = "1.0.6",
	description = "A launcher which takes a groupId, artifactId, and version and resolves the artifact with dependencies from maven central, then launches a main class",
	nickName = "maven-repository-launcher",
	versionControl = "https://bradleysmithllc@bitbucket.org/bradleysmithllc/maven-repository-launcher.git"
)
public class Launcher
{
	private String groupId;
	private String artifactId;
	private String version;
	private String className;
	private File repository;
	private String [] args;
	private String repositoryUrl;
	private String repositoryUser;
	private String repositoryPassword;

	@CLIOption(
		longName = "groupId",
		name = "gid",
		description = "The group id to pull from central",
		required = true
	)
	public void setGroupId(String groupId)
	{
		this.groupId = groupId;
	}

	@CLIOption(
		longName = "artifactId",
		name = "aid",
		description = "The artifact id to pull from central",
		required = true
	)
	public void setArtifactId(String artifactId)
	{
		this.artifactId = artifactId;
	}

	@CLIOption(
		longName = "version",
		name = "vn",
		description = "The version to pull from central.  Use 'LATEST' for the most recent release version",
		defaultValue = "LATEST"
	)
	public void setVersion(String version)
	{
		this.version = version;
	}

	@CLIOption(
			longName = "class-name",
			name = "cl",
			description = "The class to invoke the public static void main(String [] argv) method on.",
			required = true
	)
	public void setClassName(String className)
	{
		this.className = className;
	}

	@CLIOption(
		longName = "arguments",
		name = "args",
		description = "The arguments to pass to the main method.  Default is an empty array.",
		valueSeparator = ':',
		valueCardinality = CLIOption.UNLIMITED_VALUES
	)
	public void setArguments(String [] args)
	{
		this.args = args;
	}

	@CLIOption(
			longName = "local-repository",
			name = "lr",
			description = "The location for the local repository",
			required = true
	)
	public void setLocalRepository(String repo)
	{
		this.repository = new File(repo);
	}

	@CLIOption(
			longName = "repository-proxy",
			name = "rp",
			description = "The url for the local repository proxy",
			required = false
	)
	public void setRepositoryProxy(String url)
	{
		repositoryUrl = url;
	}

	@CLIOption(
			longName = "repository-username",
			name = "ruser",
			description = "The username to use to login to the proxy",
			required = false
	)
	public void setRepositoryUsername(String user)
	{
		repositoryUser = user;
	}

	@CLIOption(
			longName = "repository-password",
			name = "rpass",
			description = "The password to use to login to the proxy",
			required = false
	)
	public void setRepositoryPassword(String pass)
	{
		repositoryPassword = pass;
	}

	@CLIMain
	public void main() throws DependencyResolutionException, MalformedURLException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
		// prepare the class loader
		AetherDemo aetherDemo = repositoryUrl != null ? new AetherDemo(repository, repositoryUrl, repositoryUser, repositoryPassword) : new AetherDemo(repository);
		List<URL> urls = aetherDemo.resolve(groupId, artifactId, version);

		URLClassLoader url = new URLClassLoader(urls.toArray(new URL[urls.size()]), new ClassLoader(){
			@Override
			public Class<?> loadClass(String name) throws ClassNotFoundException {
				throw new ClassNotFoundException();
			}
		});

		Thread.currentThread().setContextClassLoader(url);

		Class cl = url.loadClass(className);

		// look up the static main method and invoke
		Method method = cl.getDeclaredMethod("main", String[].class);

		if (method == null)
		{
			throw new IllegalArgumentException("Java class " + cl.getName() + " does not declare a main(String []) method");
		}

		int modifiers = method.getModifiers();

		if (!Modifier.isStatic(modifiers))
		{
			throw new IllegalArgumentException("Java class " + cl.getName() + " declares a main(String []) method, but it isn't static");
		}

		if (!Modifier.isPublic(modifiers))
		{
			throw new IllegalArgumentException("Java class " + cl.getName() + " declares a static main(String []) method, but it isn't public");
		}

		method.invoke(null, new Object[]{args == null ? new String [] {} : args});
	}

	public static void main(String [] argv)
	{
		CommonsCLILauncher.mainClean(argv);
	}
}