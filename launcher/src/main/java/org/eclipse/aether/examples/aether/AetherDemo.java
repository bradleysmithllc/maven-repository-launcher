/*******************************************************************************
 * Copyright (c) 2010, 2012 Sonatype, Inc.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Sonatype, Inc. - initial API and implementation
 *******************************************************************************/
package org.eclipse.aether.examples.aether;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.deployment.DeploymentException;
import org.eclipse.aether.graph.DependencyNode;
import org.eclipse.aether.installation.InstallationException;
import org.eclipse.aether.resolution.DependencyResolutionException;
import org.eclipse.aether.util.artifact.SubArtifact;

@SuppressWarnings( "unused" )
public class AetherDemo
{
	public static final String HTTP_REPO_MAVEN_APACHE_ORG_MAVEN2 = "http://repo.maven.apache.org/maven2";
	private final File localRepository;
	private final String repositoryProxy;
	private final String repositoryUser;
	private final String repositoryPass;

	public AetherDemo(File localRepository, String repositoryProxy, String userName, String password) {
		this.localRepository = localRepository;
		this.repositoryProxy = repositoryProxy;
		this.repositoryUser = userName;
		this.repositoryPass = password;
	}

	public AetherDemo(File localRepository) {
		this.localRepository = localRepository;
		this.repositoryProxy = HTTP_REPO_MAVEN_APACHE_ORG_MAVEN2;
		this.repositoryUser = null;
		this.repositoryPass = null;
	}


	public List<URL> resolve(String groupId, String artifactId, String version)
			throws DependencyResolutionException, MalformedURLException
	{
		Aether aether = new Aether( repositoryProxy, repositoryUser, repositoryPass, localRepository.getAbsolutePath());

		AetherResult result = aether.resolve( groupId, artifactId, version );

		// Get the root of the resolved tree of artifacts
		//
		DependencyNode root = result.getRoot();

		// Get the list of files for the artifacts resolved
		//
		// Get the classpath of the artifacts resolved
		List<URL> urlList = new ArrayList<URL>();

		for ( File node : result.getResolvedFiles())
		{
			urlList.add(node.toURI().toURL());
		}

		return urlList;
	}
}