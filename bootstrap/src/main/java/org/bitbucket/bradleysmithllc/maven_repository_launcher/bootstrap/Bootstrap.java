package org.bitbucket.bradleysmithllc.maven_repository_launcher.bootstrap;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

public class Bootstrap {

	public static final String ORG_BITBUCKET_BRADLEYSMITHLLC_MAVEN_REPOSITORY_LAUNCHER_BOOTSTRAP_BOOTSTRAP_LAUNCHER_JAR = "org.bitbucket.bradleysmithllc.maven_repository_launcher.bootstrap.Bootstrap.launcherJar";

	public static void main(String[] args) {
		// grab the system property which locates the launcher jar file
		String prop = System.getProperty(ORG_BITBUCKET_BRADLEYSMITHLLC_MAVEN_REPOSITORY_LAUNCHER_BOOTSTRAP_BOOTSTRAP_LAUNCHER_JAR);

		if (prop == null)
		{
			throw new IllegalArgumentException("Missing system property '" + ORG_BITBUCKET_BRADLEYSMITHLLC_MAVEN_REPOSITORY_LAUNCHER_BOOTSTRAP_BOOTSTRAP_LAUNCHER_JAR + "'.  This property is required and must point to the launcher jar file.");
		}

		File file = new File(prop);

		if (!file.exists())
		{
			throw new IllegalArgumentException("Launcher jar '" + prop + "' does not point to a real file path");
		}

		if (!file.isFile())
		{
			throw new IllegalArgumentException("Launcher jar '" + prop + "' does not point to a plain file");
		}

		try {
			URLClassLoader loader = new URLClassLoader(new URL[] {file.toURI().toURL()});

			Thread.currentThread().setContextClassLoader(loader);

			Class launcherCl = loader.loadClass("org.bitbucket.bradleysmithllc.maven_repository_launcher.Launcher");

			Method method = launcherCl.getDeclaredMethod("main", String[].class);

			// let the launcher do it's work
			method.invoke(null, new Object [] {args});
		} catch (MalformedURLException e) {
			throw new RuntimeException("File can not be made into a UR: " + file);
		} catch (ClassNotFoundException e) {
			throw new IllegalArgumentException("Launcher jar " + file + " does not appear to be a maven-repository-launcher jar file");
		} catch (NoSuchMethodException e) {
			throw new IllegalArgumentException("Launcher class org.bitbucket.bradleysmithllc.maven_repository_launcher.Launcher loaded from jar file " + file + " does not appear to be a maven-repository-launcher class - missing public static void main(String [] argv)");
		} catch (InvocationTargetException e) {
			throw new IllegalArgumentException("Launcher class org.bitbucket.bradleysmithllc.maven_repository_launcher.Launcher loaded from jar file " + file + " failed in public static void main(String [] argv)", e);
		} catch (IllegalAccessException e) {
			throw new IllegalArgumentException("Launcher class org.bitbucket.bradleysmithllc.maven_repository_launcher.Launcher loaded from jar file " + file + " does not appear to be a maven-repository-launcher class - missing public static void main(String [] argv)", e);
		}
	}

	public static void setLauncherPath(File path)
	{
		System.setProperty(ORG_BITBUCKET_BRADLEYSMITHLLC_MAVEN_REPOSITORY_LAUNCHER_BOOTSTRAP_BOOTSTRAP_LAUNCHER_JAR, path.getAbsolutePath());
	}
}