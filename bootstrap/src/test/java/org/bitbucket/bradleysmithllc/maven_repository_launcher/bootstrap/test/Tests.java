package org.bitbucket.bradleysmithllc.maven_repository_launcher.bootstrap.test;

import org.bitbucket.bradleysmithllc.maven_repository_launcher.bootstrap.Bootstrap;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

public class Tests
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test(expected = IllegalArgumentException.class)
	public void missingProperty()
	{
		Bootstrap.main(new String [] {});
	}

	@Test(expected = IllegalArgumentException.class)
	public void propertyToBadPath()
	{
		Bootstrap.setLauncherPath(new File("/pmt_" + System.currentTimeMillis()));
		Bootstrap.main(new String[]{});
	}

	@Test(expected = IllegalArgumentException.class)
	public void propertyToDir() throws IOException {
		Bootstrap.setLauncherPath(temporaryFolder.newFolder());
		Bootstrap.main(new String[]{});
	}

	@Test(expected = IllegalArgumentException.class)
	public void propertyToInvalidJar() throws IOException {
		Bootstrap.setLauncherPath(temporaryFolder.newFile());
		Bootstrap.main(new String[]{});
	}

	//@Test
	public void testLaunching() throws IOException {
		Bootstrap.setLauncherPath(new File("/Users/bsmith/git/maven-repository-launcher/launcher/target/maven-repository-launcher-1.0.3-jar-with-dependencies.jar"));
		Bootstrap.main(new String[]{
			"-v",
			"-gid",
			"org.bitbucket.bradleysmithllc.etl-agent",
			"-aid",
			"server",
			"-cl",
			"org.bitbucket.bradleysmithllc.etlagent.server.EtlAgent",
			"-lr",
			temporaryFolder.newFolder().getAbsolutePath(),
			"-args",
			"@-root," + temporaryFolder.newFolder().getAbsolutePath() + ",@-v"
		});
	}
}